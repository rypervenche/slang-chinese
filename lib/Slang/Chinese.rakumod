module Slang::Chinese:ver<0.0.1> { };

use nqp;
use QAST:from<NQP>;

sub EXPORT(|) {

    my role Chinese::Grammar {

        token statement_control:sym<use> {
            [ <sym> | '用' ] <.ws> <name>
        }

        token scope_declarator:sym<my> { [ <sym> | '我的' ] <scoped('my')> }
        token package_declarator:sym<class> {
            :my $*OUTERPACKAGE := self.package;
            :my $*PKGDECL := 'class';
            :my $*LINE_NO := HLL::Compiler.lineof(self.orig(), self.from(), :cache(1));
            [ <sym> | '類別' ]<.kok> <package_def>
            <.set_braid_from(self)>
        }

		token routine_declarator:sym<sub> {
			:my $*LINE_NO := HLL::Compiler.lineof(self.orig(), self.from(), :cache(1));
			[ <sym> | '函式' ] <.end_keyword> <routine_def('sub')>
		}

		token routine_declarator:sym<method> {
			:my $*LINE_NO := HLL::Compiler.lineof(self.orig(), self.from(), :cache(1));
			[ <sym> | '方法' ] <.end_keyword> <method_def('method')>
		}

		token routine_declarator:sym<submethod> {
			:my $*LINE_NO := HLL::Compiler.lineof(self.orig(), self.from(), :cache(1));
			[ <sym> | '子方法' ] <.end_keyword> <method_def('submethod')>
		}

        token scope_declarator:sym<has>       {
            :my $*LINE_NO := HLL::Compiler.lineof(self.orig(), self.from(), :cache(1));
            [ <sym> | '有' ]
            :my $*HAS_SELF := 'partial';
            :my $*ATTR_INIT_BLOCK;
            <scoped('has')>
        }

    }

    $*LANG.define_slang: 'MAIN',
        $*LANG.slang_grammar('MAIN').^mixin(Chinese::Grammar);
        #$*LANG.actions;

    {}
}

=begin pod

=head1 NAME

Slang::Chinese - Lets you use Chinese names for almost everything.

=head1 SYNOPSIS

=begin code :lang<raku>

use Slang::Chinese;

=end code

=head1 DESCRIPTION

Slang::Chinese is ...

=head1 AUTHOR

Perry Thompson <contact@ryper.org>

=head1 COPYRIGHT AND LICENSE

Copyright 2020 Perry Thompson

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

=end pod
