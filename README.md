NAME
====

Slang::Chinese - blah blah blah

SYNOPSIS
========

```raku
use Slang::Chinese;
```

![Example](res/slang-raku.png)

DESCRIPTION
===========

Slang::Chinese is a slang that allows you to write Raku code completely in Chinese.

AUTHOR
======

Perry Thompson <contact@ryper.org>

COPYRIGHT AND LICENSE
=====================

Copyright 2020 Perry Thompson

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

