#!/usr/bin/env raku

use Slang::Chinese;
use Terminal::ANSIColor;

# English version
class Employee {
    has Str $.first-name;
    has Str $.last-name;
    has Rat $.salary;

    method full-name {
        "$!first-name $!last-name";
    }
}

my $employee = Employee.new(
    first-name => 'John',
    last-name  => 'Smith',
    salary     => 10_000.00
);

sub shout (Str $word) {
    say $word.uc;
}

shout('blah');
say $employee.full-name;
