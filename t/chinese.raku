#!/usr/bin/env raku

use Slang::Chinese;
用 Terminal::ANSIColor;

# Chinese version
類別 員工 {
    有 $.姓氏;
    有 $.名字;
    有 $.薪水;

    方法 全名 {
        "$!姓氏$!名字 ";
    }
}

我的 $員工 = 員工.new(
    姓氏 => '王',
    名字 => '力宏',
    薪水 => 10_000.00
);

函式 喊叫($單詞) {
    say $單詞.uc;
}

喊叫('blah');
say $員工.全名;
